<?php

interface Overloadable {
    public function getValue();
}