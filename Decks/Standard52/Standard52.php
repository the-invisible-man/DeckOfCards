<?php

namespace Decks\Standard52;

use Decks\Deck;
use Decks\Standard52\CardTypes\Clubs;
use Decks\Standard52\CardTypes\Diamonds;
use Decks\Standard52\CardTypes\Hearts;
use Decks\Standard52\CardTypes\Spades;
use Sorting\SortAlgo;
use Decks\Card;

class Standard52 implements Deck
{
    /**
     * @var Card[]
     */
    private $dealt      = [];

    /**
     * @var Card[]
     */
    private $notDealt   = [];

    /**
     * @var array
     */
    private $types      = [
        Clubs::class,
        Diamonds::class,
        Hearts::class,
        Spades::class
    ];

    /**
     * Base 13 system for a standard deck of cards
     * @var array
     */
    private $symbols   = [
        2   => 2,
        3   => 3,
        4   => 4,
        5   => 5,
        6   => 6,
        7   => 7,
        8   => 8,
        9   => 9,
        10  => 10,
        'J' => 11,
        'Q' => 12,
        'K' => 12,
        'A' => 14
    ];

    const SALT = 0x4 . 0x63 . 0xf2 . 0x2;

    /**
     * @var SortAlgo
     */
    private $sortAlgo;

    /**
     * Deck constructor.
     * @param SortAlgo $sortAlgo
     */
    public function __construct(SortAlgo $sortAlgo)
    {
        $this->sortAlgo = $sortAlgo;

        $this->buildDeck();
    }


    private function buildDeck()
    {
        foreach ($this->getTypes() as $type) {
            foreach ($this->getSymbols() as $naturalValue => $decimalValue) {
                $this->notDealt[] = new $type($naturalValue, $decimalValue);
            }
        }
    }

    public function shuffle()
    {
        // Because our keys are random strings we can simply
        // sort the already random strings to shuffle. We generate
        // new keys on every shuffle for the shuffle to work every time.
        //
        // We are not checking if the uid is truly unique, given
        // that the uid is a 16 byte string (128 bits), and in most cases
        // running on a 64-bit system we have a very small chance of a collision.
        $newKeys        = array_map([$this, 'getUid'], range(1, count($this->notDealt)));
        $reKeyed        = array_combine($newKeys, $this->notDealt);
        $this->notDealt = $this->sortAlgo->sortArray($reKeyed, true);
    }

    /**
     * @return string
     */
    private function getUid() : string
    {
        // We'll do our best to be as random as we can
        return bin2hex(random_bytes(16));
    }

    /**
     * @return array
     */
    public function getSymbols() : array
    {
        return $this->symbols;
    }

    /**
     * @return array
     */
    public function getTypes() : array
    {
        return $this->types;
    }

    /**
     * @return Card
     */
    public function dealOne()
    {
        $card   = end($this->notDealt);
        $key    = key($this->notDealt);
        $this->dealt[$key] = $card;

        unset($this->notDealt[$key]);
        reset($this->notDealt);

        return $card;
    }

    /**
     * @param int $amount
     * @return array
     */
    public function deal(int $amount) : array
    {
        $cards          = array_slice($this->notDealt, count($this->notDealt) - $amount);
        $this->notDealt = array_diff_key($this->notDealt, $cards);
        $this->dealt    = array_merge($this->dealt, $cards);

        return $cards;
    }

    public function print()
    {
        // Print out both dealt and not dealt cards
        echo "Total not dealt: " . count($this->notDealt) . "\n";
        foreach ($this->notDealt as $card) {
            $card->print();
            echo "\n";
        }

        echo "\n";

        echo "Total dealt: " . count($this->dealt) . "\n";
        foreach ($this->dealt as $card) {
            $card->print();
            echo "\n";
        }
    }
}