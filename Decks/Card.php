<?php

namespace Decks;

use Overloadable;

abstract class Card implements Overloadable, \JsonSerializable {

    /**
     * @var int
     */
    private $naturalValue   = null;

    /**
     * @var int
     */
    private $decimalValue   = null;

    /**
     * Card constructor.
     * @param string|int $naturalValue
     * @param int $decimalValue
     */
    public function __construct($naturalValue, int $decimalValue)
    {
        $this->naturalValue     = $naturalValue;
        $this->decimalValue     = $decimalValue;
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return $this->getNatural() . ' of ' . $this->getType();
    }

    /**
     * @return string
     */
    public function jsonSerialize()
    {
        return (string)$this;
    }

    /**
     * @return int
     */
    public function getDecimal()
    {
        return $this->decimalValue;
    }

    /**
     * @return int|string
     */
    public function getNatural()
    {
        return $this->naturalValue;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->getDecimal();
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        $type = explode('\\', get_called_class());
        return array_pop($type);
    }

    public function print()
    {
        echo (string)$this;
    }
}