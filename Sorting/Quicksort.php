<?php

namespace Sorting;

use Overloadable;

class QuickSort implements SortAlgo
{
    /**
     * In the order of O(n log n) – When $assoc = true
     * the array is sorted using its keys for comparison.
     * @param array $array
     * @param bool $assoc
     * @param bool $overload
     * @return array
     * @throws \Exception
     */
    public function sortArray(array $array, bool $assoc = false, bool $overload = false)
    {
        if(count($array) < 2) {
            return $array;
        }

        $left = $right = [];

        reset($array);

        $pivotKey   = key($array);
        $pivot      = array_shift($array);

        foreach ($array as $key => $val) {
            if(($assoc && $key < $pivotKey) || ($this->extractValue($array[$key], $overload) < $this->extractValue($pivot, $overload)))
                $left[$key]   = $val;
            else
                $right[$key]  = $val;
        }

        return array_merge($this->sortArray($left, $assoc, $overload), [$pivotKey => $pivot], $this->sortArray($right, $assoc, $overload));
    }

    /**
     * @param $object
     * @param bool $overload
     * @return mixed
     */
    public function extractValue($object, bool $overload)
    {
        if ($overload && $object instanceof Overloadable) {
            return $object->getValue();
        }

        return $object;
    }

}