<?php

namespace Traits;

trait IteratesArray {

    /**
     * @var array
     */
    protected $arrayToIterate = [];

    /**
     * @var int
     */
    protected $arrayToIterateIndex = 0;

    /**
     * @param array $array
     */
    protected function iterateOn(array &$array)
    {
        $this->arrayToIterate = &$array;
    }

    /**
     * @return mixed
     */
    public function current()
    {
        return $this->arrayToIterate[$this->arrayToIterateIndex];
    }

    /**
     * @inheritdoc
     */
    public function next()
    {
        ++$this->arrayToIterateIndex;
    }

    /**
     * @inheritdoc
     */
    public function key()
    {
        return $this->arrayToIterateIndex;
    }

    /**
     * @inheritdoc
     */
    public function valid()
    {
        return array_key_exists($this->arrayToIterateIndex, $this->arrayToIterate);
    }

    /**
     * @inheritdoc
     */
    public function rewind()
    {
        $this->arrayToIterateIndex = 0;
    }
}