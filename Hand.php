<?php

use Traits\IteratesArray;
use Decks\Card;
use Sorting\SortAlgo;

/**
 * Class Hand
 * @method Card current()
 */
class Hand implements JsonSerializable, Iterator, Countable {

    use IteratesArray;

    /**
     * @var Card[]
     */
    private $cards = [];

    /**
     * @var array
     */
    private $deckSymbols;

    /**
     * @var int
     */
    private $totalSymbols;

    /**
     * @var SortAlgo
     */
    private $sortAlgo;

    /**
     * @var array
     */
    private $typesTracker = [];

    /**
     * Dealer constructor.
     * @param SortAlgo $sortAlgo
     * @param array $deckSymbols
     */
    public function __construct(SortAlgo $sortAlgo, array $deckSymbols)
    {
        $this->deckSymbols  = $deckSymbols;
        $this->totalSymbols = count($this->deckSymbols);
        $this->sortAlgo     = $sortAlgo;

        $this->iterateOn($this->cards);
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->cards);
    }

    /**
     * @param Card $card
     * @return $this;
     */
    public function addCard(Card $card)
    {
        // We're always keeping track of the card
        // types and the total of each type
        if (!array_key_exists($card->getType(), $this->typesTracker)) {
            $this->typesTracker[$card->getType()] = 1;
        } else {
            $this->typesTracker[$card->getType()]++;
        }

        $this->cards[] = $card;

        return $this;
    }

    /**
     * @param int $length
     * @param bool $sameSuit
     * @return bool
     */
    public function hasStraight(int $length, bool $sameSuit) : bool
    {
        if (count($this) < $length) {
            return false;
        }
        // Sort cards
        $this->sortByValue();

        $currType           = null;
        $currNumber         = 0;
        $totalStraights     = 0;
        $typeDone           = [];
        $lastValue          = 0;

        foreach ($this as $card)
        {
            // Count number of occurrences while same type until we hit desired length
            if ($currType != $card->getType()) {
                $currType   = $card->getType();
                $lastValue  = $card->getValue();
                $currNumber = 1;
            } else {
                $currNumber++;

                if (!array_key_exists($card->getType(), $typeDone)) {
                    $totalStraights++;
                    $typeDone[$card->getType()] = 0;
                } elseif($lastValue == ($card->getValue() - 1)) {
                    $typeDone[$card->getType()]++;
                }
            }

            if (($sameSuit && $currNumber >= $length) || (!$sameSuit && isset($typeDone[$card->getType()]) && $typeDone[$card->getType()] >= 2)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param Card[] $cards
     * @return $this
     */
    public function addMany(array $cards)
    {
        foreach ($cards as $card) {
            $this->addCard($card);
        }
        return $this;
    }

    /**
     * @return string
     */
    public function jsonSerialize()
    {
        return json_encode($this->cards);
    }

    public function sortByValue()
    {
        // Order By Value
        $this->cards = $this->sortAlgo->sortArray($this->cards, false, true);

        // Now by suite
        $final              = range(0, array_sum($this->typesTracker) - 1);
        $tracking           = [];
        $trackedTypesTotal  = [];

        // Sorting by suit – O(n)
        foreach ($this->cards as $card)
        {
            if (!array_key_exists($card->getType(), $tracking)) {
                $key            = array_sum($trackedTypesTotal);
                $final[$key]    = $card;

                $tracking[$card->getType()]             = $key;
                $trackedTypesTotal[$card->getType()]    = $this->typesTracker[$card->getType()];
            } else {
                $tracking[$card->getType()]++;
                $final[$tracking[$card->getType()]] = $card;
            }
        }

        $this->cards    = $final;
    }

    public function sortBySuit()
    {
        $final              = range(0, array_sum($this->typesTracker) - 1);
        $tracking           = [];
        $trackedTypesTotal  = [];

        // Sorting by suit – O(n)
        foreach ($this->cards as $card)
        {
            if (!array_key_exists($card->getType(), $tracking)) {
                $key            = array_sum($trackedTypesTotal);
                $final[$key]    = $card;

                $tracking[$card->getType()]             = $key;
                $trackedTypesTotal[$card->getType()]    = $this->typesTracker[$card->getType()];
            } else {
                $tracking[$card->getType()]++;
                $final[$tracking[$card->getType()]] = $card;
            }
        }

        $this->cards    = $final;
        $final          = [];
        $trackedSorted  = [];

        // Sorting by value
        foreach ($trackedTypesTotal as $type => $total)
        {
            $start  = array_sum($trackedSorted);
            $final  = array_merge($final, $this->sortAlgo->sortArray(array_slice($this->cards, $start, $total), false, true));

            $trackedSorted[] = $total;
        }

        $this->cards = $final;
    }
}