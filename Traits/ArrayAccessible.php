<?php

namespace Traits;

trait ArrayAccessible
{
    protected $arrayToAccess;

    /**
     * @param array $array
     */
    protected function arrayAccessOn(array &$array)
    {
        $this->arrayToAccess = &$array;
    }

    /**
     * @param $offset
     * @return array
     */
    public function offsetExists($offset)
    {
        return array_key_exists($this->arrayToAccess, $offset);
    }

    /**
     * @param $offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return $this->arrayToAccess[$offset];
    }

    /**
     * @param $offset
     * @param $value
     */
    public function offsetSet($offset, $value)
    {
        $this->arrayToAccess[$offset] = $value;
    }

    /**
     * @param $offset
     */
    public function offsetUnset($offset)
    {
        unset($this->arrayToAccess[$offset]);
    }
}