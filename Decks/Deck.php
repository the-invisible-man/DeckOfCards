<?php

namespace Decks;

interface Deck
{
    public function getSymbols() : array;

    public function getTypes() : array;

    public function dealOne();

    public function print();

    public function shuffle();
}