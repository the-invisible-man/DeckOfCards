<?php

namespace Sorting;

interface SortAlgo {

    public function sortArray(array $array, bool $assoc = false, bool $overload = false);

}