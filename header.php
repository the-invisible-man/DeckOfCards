<?php

spl_autoload_register(function ($class_name) {
    $currentPath    = realpath(__DIR__);
    $path           = $currentPath . '/' . str_replace('\\', '/', $class_name) . '.php';
    if (file_exists($path)) {
        require_once $path;
        return true;
    }
    return false;
});


