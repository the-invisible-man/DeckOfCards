<?php

require_once 'header.php';

use Sorting\QuickSort;
use Decks\Standard52\Standard52;

$sort = new QuickSort();
$deck = new Standard52($sort);
$hand = new Hand($sort, $deck->getSymbols());

// Print the deck in its current state
$deck->print();

// Let's shuffle the deck
$deck->shuffle();

// Let's check it out again
$deck->print();

// Let's deal 1 card
$hand->addCard($deck->dealOne());

// We'll deal 4 more cards
$hand->addMany($deck->deal(4));


// Print again and should now be two lists, one of dealt cards
$deck->print();

// Sort by suit
$hand->sortBySuit();



echo "\n\nSorted Hand by suit:\n";

// Print the hand
foreach ($hand as $card) {
    $card->print();
    echo "\n";
}

echo "\n\nSorted Hand by value:\n";

// Sort by value
$hand->sortByValue();

// Print again
foreach ($hand as $card) {
    $card->print();
    echo "\n";
}

// Do we have a straight hand?
echo "\nStraights of any hand:";
if ($hand->hasStraight(2, false)) {
    echo "\nYeah straights!";
} else {
    echo "\nNope\n";
}

echo "\nFlush:";
if ($hand->hasStraight(5, true)) {
    echo "\nFLUSH!!\n";
} else {
    echo "\nNope\n";
}

echo "\nUsing new deck, not shuffled";

// We can force a flush by creating another deck and not shuffling it
$deck = new Standard52($sort);
$hand = new Hand($sort, $deck->getSymbols());

// We'll deal 5 cards
$hand->addMany($deck->deal(5));

echo "\nFlush:";
if ($hand->hasStraight(5, true)) {
    echo "\nFLUSH!!\n";
} else {
    echo "\nNope\n";
}
